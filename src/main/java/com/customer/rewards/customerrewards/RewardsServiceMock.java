package com.customer.rewards.customerrewards;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.customer.rewards.model.Customer;
import com.customer.rewards.model.MyTransaction;

@Service
public class RewardsServiceMock {

	private static List<MyTransaction> transactions = new ArrayList<MyTransaction>();
	private static long index;
	
	static {
		
		transactions.add( new MyTransaction(index++, new Customer(1, "Anoop"), 100.0, "Purchase 1", new Date()) );
		transactions.add( new MyTransaction(index++, new Customer(2, "SaiAshay"), 50.0, "Purchase 2", new Date()) );
		transactions.add( new MyTransaction(index++, new Customer(3, "Anjali"), 120.0, "Purchase 3", new Date()) );
	
	}
	
	
	public List<MyTransaction> getAll() {
		return transactions;
	}
	
}
