insert into customer(id, name) values (1000, 'Anoop');
insert into customer(id, name) values (1001, 'SaiAkshay');
insert into customer(id, name) values (1002, 'Anjali');

insert into my_transaction(id, description, total, save_date, customer_id) values (1001, 'Purchase 1', 100, '2022-01-17 11:30:55', 1000);
insert into my_transaction(id, description, total, save_date, customer_id) values (1002, 'Purchase 2', 50, '2022-01-01 15:20:10', 1000);
insert into my_transaction(id, description, total, save_date, customer_id) values (1003, 'Purchase 4', 120, '2021-12-10 13:20:10', 1000);
insert into my_transaction(id, description, total, save_date, customer_id) values (1004, 'Purchase 10', 175.32, '2021-11-07 12:20:10', 1000);
insert into my_transaction(id, description, total, save_date, customer_id) values (1005, 'Purchase 20', 65.75, '2019-10-05 10:30:10', 1000);
insert into my_transaction(id, description, total, save_date, customer_id) values (1006, 'Purchase 25', 210.50, '2018-08-06 14:50:10', 1000);
insert into my_transaction(id, description, total, save_date, customer_id) values (1009, 'Purchase 15', 42.80, '2018-09-23 16:05:30', 1000);

insert into my_transaction(id, description, total, save_date, customer_id) values (1200, 'Purchase 300', 25.60, '2022-01-11 14:40:20', 1002);
insert into my_transaction(id, description, total, save_date, customer_id) values (1201, 'Purchase 101', 80.50, '2021-10-05 13:00:30', 1002);
insert into my_transaction(id, description, total, save_date, customer_id) values (1202, 'Purchase 202', 116.14, '2018-07-10 15:50:50', 1002);

insert into my_transaction(id, description, total, save_date, customer_id) values (100, 'Purchase 10', 200, '2022-01-11 12:25:30', 1001);

